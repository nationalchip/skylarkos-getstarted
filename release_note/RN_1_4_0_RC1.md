### SkylarkOS V1.4.0-rc1
##### released 2019-12-31

***

#### 发布内容:
- openwrt
- uboot
- kernel
- skylark
- middleware
- 发布信息(本文件)

***

#### 更新内容:
此版本的语音前处理，KWS功能加入了密钥保护，需获取授权才可使用；

在Skylark Js App SDK上重新实现了原senseflow app各应用方案，原senseflow app不再维护；

系统更名为SkylarkOS；

##### App:
- 更新 Skylark Js App SDK v1.4.2
- 重构framework，分离事件通道与具体的业务逻辑
- 添加Azero Solution
- 添加离线Demo：热水器、空调
- 添加Public Log服务
- hw加载底层模块，添加判断引擎是否包含相应模块的接口，结合配置文件配置的方式加载

##### Middleware:
- 重构player，对外提供MediaPlayer和AudioTrack接口，支持数据流播放
- network service: 支持保存历史热点信息，自动选择或切换合适的热点
- bluetooth service：支持自定义BLE UUID
- power manager: 添加软关机接口
- ota：重构ota服务，由服务接管ota流程，应用可通过查询或监听接口获取ota状态
- 支持本地通过usb或sd卡升级固件
- 添加factory组件
- 添加libserial

##### System:
- VSP加入密钥保护
- 优化KWS唤醒性能
- 提高rfkill加载优先级
- 休眠唤醒后重置pwm状态
- 修复休眠唤醒后spi timeout
- 修复camera休眠问题
- 解决蓝牙播放音乐中，触发按键会出现丢包卡顿问题
- 添加GD SPI NAND GD5F2GQ5UEYIG
- HCI: fix skb request function __hci_req_sync which leaks
- alsa修改音量控制范围（-20 ～ 18 db）
- 修正ck i2c寄存器地址
- 优化启动速度
- 更新 bootx V1.5.3

##### BSP:
- 精简板级配置
- 添加gx8009b november 1v板级

